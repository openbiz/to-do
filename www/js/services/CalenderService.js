(function() {
    TODO.service("CalenderService",[
        function() {

            this.getDayArray = function getDayArray(year, month) {
            
                month = month == 12 ? 0 : month;
                month = month == -1 ? 11 : month;
            
                var date = new Date(year, month, 1);
                var dayOfWeek = date.getDay();
                
                var dayObj = {
                    "preMonth" : {
                        "days" : [],
                        "weeks": [],
                        "tasks" : []
                    },
                    "activeMonth" : {
                        "days" : [],
                        "weeks": [],
                        "tasks" : []
                    },
                    "nextMonth" : {
                        "days" : [],
                        "weeks": [],
                        "tasks" : []
                    }
                };
                
                switch (month) {
                    case 0:
                    case 7:
                    case 12:
                        buildPrefix(dayObj.preMonth, 31, dayOfWeek);
                        buildMain(dayObj.activeMonth, 31, year, month);
                        break;
                    case 1:
                        if (isLeapYear(year)) {
                            buildPrefix(dayObj.preMonth, 31, dayOfWeek);
                            buildMain(dayObj.activeMonth, 29, year, month);
                        } else {
                            buildPrefix(dayObj.preMonth, 31, dayOfWeek);
                            buildMain(dayObj.activeMonth, 28, year, month);
                        }                        
                        break;
                    case 2:
                        if (isLeapYear(year)) {
                            buildPrefix(dayObj.preMonth, 29, dayOfWeek);
                            buildMain(dayObj.activeMonth, 31, year, month);
                        } else {
                            buildPrefix(dayObj.preMonth, 28, dayOfWeek);
                            buildMain(dayObj.activeMonth, 31, year, month);
                        }                        
                        break;
                    case 3:
                    case 5:
                    case 8:
                    case 10:
                        buildPrefix(dayObj.preMonth, 31, dayOfWeek);
                        buildMain(dayObj.activeMonth, 30, year, month);
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                    case -1:
                        buildPrefix(dayObj.preMonth, 30, dayOfWeek);
                        buildMain(dayObj.activeMonth, 31, year, month);
                        break;
                }
                
                buildSuffix(dayObj.nextMonth, dayObj.preMonth.days.length + dayObj.activeMonth.days.length);
                
                return dayObj;
            };
            
            this.getMonth = function getMonth(month) {
                switch (month) {
                    case -1:
                    case 11:
                    case -2:
                        return "Dec";
                    case 0:
                    case 12:
                        return "Jan.";
                    case 1:
                    case 13:
                        return "Feb.";
                    case 2:
                        return "Mar.";
                    case 3:
                        return "Apr.";
                    case 4:
                        return "May";
                    case 5:
                        return "Jun.";
                    case 6:
                        return "Jul.";
                    case 7:
                        return "Aug.";
                    case 8:
                        return "Sep.";
                    case 9:
                        return "Oct.";
                    case 10:
                        return "Nov.";
                }
            };
            
            this.getActiveMonth = function getActiveMonth(month) {
                switch (month) {
                    case 0:
                    case 12:
                        return "January";
                    case 1:
                        return "February";
                    case 2:
                        return "March";
                    case 3:
                        return "April";
                    case 4:
                        return "May";
                    case 5:
                        return "June";
                    case 6:
                        return "July";
                    case 7:
                        return "August";
                    case 8:
                        return "September";
                    case 9:
                        return "October";
                    case 10:
                        return "November";
                    case 11:
                    case -1:
                        return "December";
                }
            };
            
            function isLeapYear(year) {
                return !(year % (year % 100 ? 4 : 400));
            }
            
            function buildPrefix(array, totalDay, dayOfWeek) {
                if (dayOfWeek != 0) {
                    for (var i = totalDay + 1 - dayOfWeek; i <= totalDay; i++) {
                        array.days.push(i);

                        var tmpWeeksArray = new Array();
                        array.weeks.push(tmpWeeksArray);
                        
                        var tmpTasksArray = new Array();
                        array.tasks.push(tmpTasksArray);
                    }
                }                
            }
            
            function buildMain(array, totalDay, year, month) {
                for (var i = 1; i <= totalDay; i++) {
                    array.days.push(i);

                    array.weeks.push(getWeekNo(year, month, i));
                    
                    var tmpTasksArray = new Array();
                    array.tasks.push(tmpTasksArray);
                }
            }
            
            function buildSuffix(array, len) {
                for (var i = 1; i <= 42 - len; i++) {
                    array.days.push(i);

                    var tmpWeeksArray = new Array();
                    array.weeks.push(tmpWeeksArray);
                    
                    var tmpTasksArray = new Array();
                    array.tasks.push(tmpTasksArray);
                }
            }

            function getWeekNo(year, month, day) {
                var date = new Date(year, month, day);
                switch (date.getDay()) {
                    case 0:
                        return "Sunday";
                    case 1:
                        return "Monday";
                    case 2:
                        return "Tuesday";
                    case 3:
                        return "Wednesday";
                    case 4:
                        return "Thursday";
                    case 5:
                        return "Friday";
                    case 6:
                        return "Saturday";
                }
            }
        }
    ])
})();