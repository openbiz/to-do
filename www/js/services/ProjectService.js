(function() {

	TODO.service("ProjectService", [
		"$q", "socket", "UserService", "DBService",
		function($q, socket, UserService, DBService) {

			this.createOpportunity = function createOpportunity(data) {
				var deferred = $q.defer();

				data.customerID = UserService.currentUser;
				data.manager = UserService.currentUser;
				data.customerRequirements = "Customer's requirements.";

				socket.emit("task:action", {
                    type: "opportunity",
                    action: "create",
                    data: data
                });

                socket.once("system:error", function(data) {
                    console.log(data);
                });

                deferred.resolve(data);

                return deferred.promise;
			};

			this.getProjectListFromLocalStorage = function getProjectListFromLocalStorage(username) {
				return DBService.getAllProject(username);
			};

		}
	]);

})();