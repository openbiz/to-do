(function() {

    // TODO: create a promise util that wraps $q and resolves/rejects within $timeout and $rootScope.$apply so that we don't need to rely on events
    // http://jimhoskins.com/2012/12/17/angularjs-and-apply.html
    // https://docs.angularjs.org/error/$rootScope/inprog?p0=$digest

    TODO.service("UserService", [
        "$rootScope", "$q", "socket", "$location", "$http", "appConstants", "db",
        function($rootScope, $q, socket, $location, $http, appConstants, db) {

            var KEY_USER = appConstants.userStore.keyPrefix;
            var KEY_TOKEN = appConstants.userStore.keyPrefix;

            var self = this;

            // (IMPORTANT) we should only listen to each socket event _once_ and execute poped callbacks queue
            var sessionRetrieveDeferred;

            self.login = function login(username, password) {
                var deferred = $q.defer();

                if (!username || !password) {
                    deferred.reject("Either username or password is undefined.");
                } else {
                    // set
                    sessionRetrieveDeferred = deferred;

                    $http.post(appConstants.socket.url + "/api/login", {
                        username: username,
                        password: password
                    }).success(function(response) {
                        var token = response.token;
                        var user = response.user;

                        if (token && user) {
                            socket.connect(token); // Connect to a socket

                            socket.on("connect", function() {
                                sessionRetrieveDeferred.resolve(token);
                                // decoupled event
                                $rootScope.$broadcast("user:update");
                            });

                            db.setItem(appConstants.userStore.tokenKey + username, token);
                            db.setItem(appConstants.userStore.userKey + username, JSON.stringify(user));

                        } else {
                            sessionRetrieveDeferred.reject(response.message || "Either token or user is undefined in server response");
                        }
                    })
                }

                return deferred.promise;
            };

            self.isLoggedIn = function isLoggedIn() {
                return !!db.getItem(KEY_TOKEN);
            };

            self.handshake = function handshake() {
                var deferred = $q.defer();

                if (!self.isLoggedIn()) {
                    deferred.reject("No user has logged in yet.");
                } else {
                    // set
                    sessionRetrieveDeferred = deferred;

                    // TODO: Need to make a proper handshake method (should pass in token to verify)
                    socket.on("auth:handshake", function(data) {
                        console.log("Handshake received. Start connecting to the socket");
                        socket.connect(db.getItem(KEY_TOKEN));
                        socket.on("connect", function(){
                            sessionRetrieveDeferred.resolve("Connected");
                        })
                    });

                    // Perform a handshake
                    socket.emit("auth:handshake", {check: "Hello"});
                }

                return deferred.promise;
            };

            // TODO: Should probably store an instance of the users in the local db
            self.getInfoById = function getInfoById(id){
                var deferred = $q.defer();

                socket.once("user:info", function(data){
                    deferred.resolve(data);
                });

                socket.emit("user:info",{
                    type:"single",
                    action:"retrieve",
                    user: id
                });

                return deferred.promise;
            }
            self.getInfoByIds = function getInfoByIds(ids){
                var deferred = $q.defer();

                socket.once("user:info", function(data){
                    deferred.resolve(data);
                });

                socket.emit("user:info",{
                    type:"list",
                    action:"retrieve",
                    user_list: ids
                });

                return deferred.promise;
            }
            
            self.getCurrentUser = function getCurrentUser() {
                var tmpStr = db.getItem(KEY_USER);
                return tmpStr.isJSON ? JSON.parse() : tmpStr;
            };

            self.logout = function logout() {
                var deferred = $q.defer();

                if (!self.isLoggedIn()) {
                    deferred.reject("No user has logged in yet.");
                } else {
                    // emit to server
                    socket.emit("disconnect");

                    //$cookieStore.remove(KEY_TOKEN);
                    //$cookieStore.remove(KEY_USER);
                    db.removeItem(appConstants.userStore.tokenKey + this.currentUser);
                    db.removeItem(appConstants.userStore.userKey + this.currentUser);
                    $rootScope.$broadcast("user:logout");
                    deferred.resolve(true);
                }

                navigator.app.exitApp();

                return deferred.promise;
            };

        }
    ]);

})();