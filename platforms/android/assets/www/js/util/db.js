(function() {

    /**
     * This is our repository.
     * On load it should start sync with the server.
     */
    TODO.factory("db", [
        "$rootScope", "appConstants",
        function($rootScope, appConstants) {

            var db = window.localStorage;
            
            return db;
        }
    ]);
    
})();