(function() {
    TODO.controller("TaskDetailCtrl", [
        "$rootScope", "$scope", "TaskService", "$location", "ProjectService", "UserService",
        function($rootScope, $scope, TaskService, $location, ProjectService, UserService) {

            $scope.isShowMenu = false;

        	$scope.activeTask = TaskService.activeTask;

            $scope.subTasks = $scope.activeTask.data.subTasks;
            $scope.subTasksAmount = $scope.subTasks.length;
            $scope.subTasksCompletedAmount = 0;

            $scope.dueDate = moment(Number($scope.activeTask.data.dueDate)).format("DD MMMM, YYYY");

            $scope.$on("task:update", function(event, data) {
                TaskService.activeTask = data;
            });

            for (var i = 0; i < $scope.subTasks.length; i++) {
                if ($scope.subTasks[i].status == "d") {
                    $scope.subTasksCompletedAmount++;
                }
            }

            if ($scope.subTasksAmount != 0 && $scope.subTasksCompletedAmount != 0) {
                $scope.completeStatus = $scope.subTasksCompletedAmount / $scope.subTasksAmount * 100;
            } else {
                $scope.completeStatus = 0;
            }

            
        	// This is a default value
            $scope.selectedStatusItem = "";

            switch ($scope.activeTask.status) {
                case "tbp":
                case "To Be Pulled":
                    $scope.selectedStatusItem = "To Be Pulled";
                    break;
                case "p":
                case "Pulled":
                    $scope.selectedStatusItem = "Pulled";
                    break;
                case "ip":
                case "In progress":
                    $scope.selectedStatusItem = "In progress";
                    break;
                case "d":
                case "Done":
                    $scope.selectedStatusItem = "Done";
                    break;
            }

            $scope.taskStatusList = [
            	"To Be Pulled", "Pulled", "In progress", "Done"
            ];

        	$scope.onItemClick = function onItemClick(status) {
                $scope.selectedStatusItem = status;
                $scope.activeTask.status = status;

                $scope.isShowMenu = !$scope.isShowMenu;
            };

            $scope.updateTask = function updateTask() {

                switch ($scope.selectedStatusItem) {
                    case "To Be Pulled":
                    case "tbp":
                        $scope.activeTask.status = "tbp";
                        break;
                    case "Pulled":
                    case "p":
                        $scope.activeTask.status = "p";
                        break;
                    case "In progress":
                    case "ip":
                        $scope.activeTask.status = "ip";
                        break;
                    case "Done":
                    case "d":
                        $scope.activeTask.status = "d";
                        break;
                }

                $scope.activeTask.data.dueDate
                    = moment($scope.dueDate, "DD MMMM, YYYY").hour(23).minute(59).second(59).valueOf();

                if ($scope.selectedProjectItem.length != 0) {
                    $scope.activeTask.data.lane = $scope.selectedProjectItem;
                    $scope.activeTask.pojectID = ProjectService.projectList[$scope.selectedProjectItem]._id; 
                }

                if (typeof(UserService.userList[$scope.activeTask.data.taskOwner]) != "undefined") {
                    $scope.activeTask.customerID = UserService.userList[$scope.activeTask.data.taskOwner]._id;
                }

                TaskService.updateTask($scope.activeTask);

                $location.url("/pb");
            };

            $scope.isShowProjectMenu = false;

            // This is a default value
            $scope.selectedProjectItem = $scope.activeTask.data.lane;

            $scope.onProjectItemClick = function onProjectItemClick(project) {
                $scope.selectedProjectItem = project;
                $scope.activeTask.data.lane = project;

                $scope.isShowProjectMenu = !$scope.isShowProjectMenu;
            };

            if (typeof(ProjectService.projectList) != "undefined") {
                $scope.pushToProjectArray = [];
                for (var item in ProjectService.projectList) {
                    $scope.pushToProjectArray.push(item);
                }
            };

            $scope.changeTaskOwner = function changeTaskOwner() {

                TaskService.activeTask.description = $scope.activeTask.description;
                TaskService.activeTask.data.notes = $scope.activeTask.data.notes;

                UserService.setSelectedUserFrom = "TASKDETAIL";
                $location.url("/ms");
            };

        }
    ]);
})();