(function() {

    TODO.controller("PersonalBoardCtrl", [
        "$scope", "$location", "TaskService", "UserService", "appConstants", "ProjectService", "SyncServerService",
        function($scope, $location, TaskService, UserService, appConstants, ProjectService, SyncServerService) {

            /*
             * The UserService.userList's structure should be:
             *      {
             *          user1_display_name : {_id : id, display_name : display_name},
             *          user2_display_name : {_id : id, display_name : display_name},
             *          ...
             *      }
             */
            $scope.$on("user:list", function(event, data) {
                UserService.userList = {};
                for (var i = 0; i < data.length; i++) {
                    UserService.userList[data[i].display_name] = data[i];
                }

                // TODO: when there are very many users
                $scope.assignToUsers = [];
                for (var item in UserService.userList) {
                    $scope.assignToUsers.push(item);
                }
            });

            $scope.$on("projects:update", function(event, data) {
                ProjectService.projectList = {};
                for (var i = 0; i < data.length; i++) {
                    ProjectService.projectList[data[i].name] = data[i];
                }
            });

            $scope.$on("personalTasks:update", function(event, data) {
                TaskService.tasksByLane = TaskService.buildTasksByLaneFromBroadcast(data);

                $scope.lanes = TaskService.tasksByLane.lanes;
                $scope.activeLane = $scope.lanes[0];
                $scope.activeTasks = TaskService.tasksByLane[$scope.activeLane];

                $scope.tasksAmountInToday = TaskService.getTasksAmountForToday();
            });

            if (typeof(UserService.userList) == "undefined") {
                if (appConstants.userDataFirstLoadTag) {
                    SyncServerService.retrieveUserData();
                }
            }

            if (typeof(ProjectService.projectList) == "undefined") {
                if (appConstants.projectDataFirstLoadTag) {
                    SyncServerService.retrieveProjectData();
                }
            }

            if (typeof(TaskService.tasksByLane) == "undefined") {
                if (appConstants.taskDataFirstLoadTag) {
                    SyncServerService.retrieveTaskData();
                } else {
                    TaskService.tasksByLane = TaskService.buildTasksByLaneFromLocalStorage(UserService.currentUser);
                }
            }

            if (typeof(UserService.userList) != "undefined") {
                appConstants.userDataFirstLoadTag = false;

                $scope.assignToUsers = [];
                for (var item in UserService.userList) {
                    $scope.assignToUsers.push(item);
                }
            }

            if (typeof(ProjectService.projectList) != "undefined") {
                appConstants.projectDataFirstLoadTag = false;
            }

            if (typeof(TaskService.tasksByLane) != "undefined") {
                appConstants.taskDataFirstLoadTag = false;

                $scope.lanes = TaskService.tasksByLane.lanes;
                $scope.activeLane = $scope.lanes[0];
                $scope.activeTasks = TaskService.tasksByLane[$scope.activeLane];

                $scope.tasksAmountInToday = TaskService.getTasksAmountForToday();
            }

            /*---------------------------------I am a cute divider---------------------------------*/

            if (typeof(UserService.isForTaskCreation) != "undefined") {
                if (UserService.isForTaskCreation) {
                    $scope.isShowCreateTaskBox = true;
                } else {
                    $scope.isShowCreateTaskBox = false;
                }
            } else {
                $scope.isShowCreateTaskBox = false;
            }

            if (typeof(TaskService.taskOwner) != "undefined") {
                $scope.taskOwner = TaskService.taskOwner;
            } else {
                $scope.taskOwner = UserService.currentUser;
            }

            if (TaskService.tmpUserInput != null) {
                $scope.data = TaskService.tmpUserInput;
            } else {
                $scope.data = {
                    title: "",
                    description: ""
                };
            }            

            $scope.project = {
                description: ""
            };

            $scope.createProject = function createProject() {
                ProjectService.createOpportunity($scope.project).then(
                    function success() {

                    }
                );
            };

            $scope.createTask = function createTask() {

                if (!$scope.data.title) {
                    return;
                }

                if (typeof(UserService.userList[$scope.taskOwner]) != "undefined") {
                    $scope.data["customerID"] = UserService.userList[$scope.taskOwner]._id;
                }

                $scope.data.taskOwner = $scope.taskOwner;

                if ($scope.activeLane != "TO DO" && $scope.activeLane != "This Week"
                    && $scope.activeLane != "Today" && $scope.activeLane != "Done") {
                    $scope.data.lane = $scope.activeLane;

                    $scope.data.pojectID = ProjectService.projectList[$scope.activeLane]._id;
                }

                TaskService.createTask($scope.data).then(
                    function success() {
                        // TODO
                    }
                );
            };
        
            $scope.titleTpl = {url: "templates/nav.title.tpl.html"};
            $scope.taskListTpl = {url: "templates/nav.task.list.detail.tpl.html"};            
            
            $scope.refresh = function refresh() {
                if (typeof(task) != "undefined" && task != null) {
                    $scope.activeLane = "TO DO";
                    $scope.activeTasks = TaskService.tasksByLane["TO DO"];
                }                
            };

            $scope.$on("task:create", $scope.refresh);

            $scope.changeTaskListView = function changeTaskListView() {
                $scope.taskListTpl.url = 
                    $scope.taskListTpl.url == "templates/nav.task.list.detail.tpl.html" ?
                    "templates/nav.task.list.simple.tpl.html" : "templates/nav.task.list.detail.tpl.html"
            };

            $scope.goPrevLane = function goPrevLane() {
                $scope.lanes.unshift($scope.lanes.pop());
                $scope.activeLane = $scope.lanes[0];
                $scope.activeTasks = TaskService.tasksByLane[$scope.activeLane];
            };

            $scope.goNextLane = function goNextLane() {
                $scope.lanes.push($scope.lanes.shift());
                $scope.activeLane = $scope.lanes[0];
                $scope.activeTasks = TaskService.tasksByLane[$scope.activeLane];
            };

            $scope.switchToLane = function switchToLane(laneName) {
                $scope.activeLane = laneName;
                $scope.activeTasks = TaskService.tasksByLane[$scope.activeLane];
            };

            $scope.setActiveTask = function setActiveTask(index) {
                TaskService.activeTask = $scope.activeTasks[index];
            };

            $scope.goToTwoLanesView = function goToTwoLanesView() {
                $scope.titleTpl = {url: ""};
                $scope.taskListTpl.url = "templates/no.nav.two.lane.task.summary.tpl.html";
            };

            $scope.selectTaskOwner = function selectTaskOwner() {
                UserService.setSelectedUserFrom = "TASKCREATE";
                UserService.isForTaskCreation = true;

                TaskService.tmpUserInput = $scope.data;

                $location.url("/ms");
            };

            $scope.clearTaskCreationScene = function clearTaskCreationScene() {
                UserService.isForTaskCreation = false;
                $scope.isShowCreateTaskBox = false;
                TaskService.tmpUserInput = null;
            };

            /*---------------------------------I am also a cute divider---------------------------------*/
            
        }]);

})();