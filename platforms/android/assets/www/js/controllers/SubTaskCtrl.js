(function() {
    TODO.controller("SubTaskCtrl", [
        "$scope", "TaskService", "UserService", "$location",
        function($scope, TaskService, UserService, $location) {

            if (typeof(TaskService.taskOwner) != "undefined") {
                $scope.taskOwner = TaskService.taskOwner;
            } else {
                $scope.taskOwner = UserService.currentUser;
            }

            if (UserService.isForChangeToMainTask) {
                $scope.isShowChangeSubTaskBox = true;
            } else {
                $scope.isShowChangeSubTaskBox = false;
            }

            $scope.activeTask = TaskService.activeTask;
            $scope.subTasks = $scope.activeTask.data.subTasks;
            $scope.subTasksAmount = $scope.subTasks.length;
            $scope.completedSubTasksAmount = 0;
            for (var i = 0; i < $scope.subTasks.length; i++) {
                if ($scope.subTasks[i].status == "c") {
                    $scope.completedSubTasksAmount++;
                }
            }

        	$scope.subTaskName = null;

        	$scope.addSubTasks = function addSubTasks() {
        		$scope.subTasks.push(
        			{
        				name: $scope.subTaskName,
        				status: "tbp"
        			}
        		);
        		$scope.activeTask.subTasks = $scope.subTasks;
                $scope.subTasksAmount = $scope.subTasks.length;
        		TaskService.updateTask($scope.activeTask);
        	};

        	$scope.deleteSubTask = function deleteSubTask(index) {
        		$scope.subTasks.splice(index, 1);
        		$scope.activeTask.subTasks = $scope.subTasks;
                $scope.subTasksAmount = $scope.subTasks.length;
        		TaskService.updateTask($scope.activeTask);
        	};

            $scope.activeSubTaskIndex = -1;
            $scope.showChangeSubTaskBox = function showChangeSubTaskBox(index) {
                $scope.activeSubTaskIndex = index;
                $scope.isShowChangeSubTaskBox = true;
            };

            $scope.updateSubTasks = function updateSubTasks() {
                $scope.activeTask.data.subTasks = $scope.subTasks;
                TaskService.updateTask($scope.activeTask);
            };

            if (TaskService.tmpUserInput != null) {
                $scope.data = TaskService.tmpUserInput;
            } else {
                $scope.data = {
                    title: "",
                    description: ""
                };
            }

            $scope.changeSubTaskToMainTask = function changeSubTaskToMainTask() {

                $scope.deleteSubTask($scope.activeSubTaskIndex);

                if (typeof(UserService.userList[$scope.taskOwner]) != "undefined") {
                    $scope.data["customerID"] = UserService.userList[$scope.taskOwner]._id;
                }

                $scope.data.taskOwner = $scope.taskOwner;
                
                $scope.data.title = $scope.data.description;

                TaskService.createTask($scope.data);

                $location.url("/pb");
            };

            $scope.selectTaskOwner = function selectTaskOwner() {
                UserService.setSelectedUserFrom = "SUBTASK";
                UserService.isForChangeToMainTask = true;

                TaskService.tmpUserInput = $scope.data;

                $location.url("/ms");
            };

            $scope.clearChangeToMainTaskScene = function clearChangeToMainTaskScene() {
                UserService.isForChangeToMainTask = false;
                $scope.isShowChangeSubTaskBox = false;
                TaskService.tmpUserInput = null;
            };

        }
    ]);
})();