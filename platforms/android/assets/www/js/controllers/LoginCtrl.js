(function() {

    TODO.controller("LoginCtrl", [
        "$scope", "$location", "UserService", "SyncServerService",
        function($scope, $location, UserService, SyncServerService) {

            $scope.username = null;
            $scope.password = null;

            $scope.login = function login() {
                if (!$scope.username || !$scope.password) {
                    return;
                }

                UserService.login($scope.username, $scope.password).then(

                    function success() {

                        UserService.currentUser = $scope.username;

                        SyncServerService.syncTaskDataFromServer($scope.username);                        

                        SyncServerService.syncProjectDataFromServer($scope.username);

                        SyncServerService.syncUserDataFromServer();

                        $location.url("/pb");

                    }

                );
            };

        }

    ]);

})();