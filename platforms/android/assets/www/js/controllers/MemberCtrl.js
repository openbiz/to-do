(function() {
    TODO.controller("MemberCtrl", [
        "$scope", "$location", "UserService", "TaskService",
        function($scope, $location, UserService, TaskService) {

            var selectedUserDisplayName = "";

        	$scope.allUserArray = [];
            for (var item in UserService.userList) {
                $scope.allUserArray.push(item);
            }

            $scope.activeIx = -1;

            $scope.setActiveUser = function setActiveUser(index, user) {
            	$scope.activeIx = index;
            	selectedUserDisplayName = user;
            };

            $scope.setSelectedUser = function setSelectedUser() {

                if (typeof(UserService.setSelectedUserFrom) != "undefined") {
                    if (UserService.setSelectedUserFrom == "TASKCREATE") {
                        TaskService.taskOwner = selectedUserDisplayName;
                        $location.url("/pb");
                    }

                    if (UserService.setSelectedUserFrom == "TASKDETAIL") {
                        TaskService.activeTask.data.taskOwner = selectedUserDisplayName;
                        $location.url("/td");
                    }

                    if (UserService.setSelectedUserFrom == "SUBTASK") {
                        TaskService.taskOwner = selectedUserDisplayName;
                        $location.url("/st");
                    }
                }
                
            };
        
        }
    ])
})();