(function() {
    TODO.controller("CalenderCtrl", [
        "$scope", "$location", "CalenderService", "TaskService",
        function($scope, $location, CalenderService, TaskService) {

            $scope.$on("personalTasks:update", function(event, data) {
                TaskService.tasksByLane = TaskService.buildTasksByLaneFromBroadcast(data);
            });

            $scope.activeTaskIndex = -1;
        
            var currentDate = new Date();
            
            $scope.currentMonth = currentDate.getMonth();            
            $scope.currentYear = currentDate.getFullYear();
            $scope.activeMonth = CalenderService.getActiveMonth($scope.currentMonth);
            $scope.nextMonth = CalenderService.getMonth($scope.currentMonth + 1);
            $scope.lastMonth = CalenderService.getMonth($scope.currentMonth - 1);
            
            $scope.addNewTaskTPL = {
                "url" : "templates/add.new.task.tpl.html"
            };
            
            $scope.taskSummaryBarTPL = {
                "url" : "templates/task.summary.bar.tpl.html"
            };
            
            $scope.taskSummaryListTPL = {
                "url" : "templates/calender.task.list.simple.tpl.html"
            };
            
            $scope.calenderDayTPL = {
                "url" : "templates/calender.widget.day.tpl.html"
            };
            
            $scope.taskEditBarTPL = {
                "url" : ""
            };

            $scope.taskEditBarTop = {
                "top" : "775px"
            };
            
            $scope.activeDayIx = -1;
            $scope.activeDay = 1;

            var tasks = TaskService.tasksByLane["TO DO"];

            $scope.tasksInActiveDay = [];
            $scope.tasksInActiveMonth = [];
            $scope.tasksForEveryDayInActiveMonth = [];

            $scope.tasksForEveryDayInActiveMonth = CalenderService.getDayArray($scope.currentYear, $scope.currentMonth);
            $scope.tasksForEveryDayInActiveMonth = TaskService.buildTaskDayRelation(
                $scope.tasksForEveryDayInActiveMonth,
                tasks,
                $scope.currentYear,
                $scope.currentMonth + 1
            );
            $scope.tasksInActiveDay = $scope.tasksForEveryDayInActiveMonth.activeMonth.tasks[$scope.activeDay - 1];

            $scope.getTasksAmountForActiveMonth = function getTasksAmountForActiveMonth() {
                for (var i = 0; i < $scope.tasksForEveryDayInActiveMonth.activeMonth.tasks.length; i++) {
                    $scope.tasksAmountForActiveMonth += $scope.tasksForEveryDayInActiveMonth.activeMonth.tasks[i].length;
                }
            };

            $scope.$on("task:create", function(event, data) {
                 $scope.tasksInActiveDay.push(data);
            });
            
            $scope.goNextMonth = function goNextMonth() {
            
                if ($scope.currentMonth == 11) {
                    $scope.currentYear += 1;
                }
                
                $scope.activeMonth = CalenderService.getActiveMonth($scope.currentMonth + 1);
                $scope.nextMonth = CalenderService.getMonth($scope.currentMonth + 2);
                $scope.lastMonth = CalenderService.getMonth($scope.currentMonth);
                
                $scope.currentMonth = $scope.currentMonth == 11 ? 0 : $scope.currentMonth + 1;
                
                tasks = TaskService.tasksByLane["TO DO"];
                $scope.tasksForEveryDayInActiveMonth = CalenderService.getDayArray($scope.currentYear, $scope.currentMonth);
                $scope.tasksForEveryDayInActiveMonth = TaskService.buildTaskDayRelation(
                    $scope.tasksForEveryDayInActiveMonth, tasks, $scope.currentYear, $scope.currentMonth + 1
                );                
                
                $scope.activeDay = 1;
                $scope.activeDayIx = -1;

                $scope.tasksInActiveDay = $scope.tasksForEveryDayInActiveMonth.activeMonth.tasks[$scope.activeDay - 1];

                $scope.taskEditBarTPL = {
                    "url" : ""
                };
                $scope.taskEditBarTop = {
                    "top" : "775px"
                };

                $scope.tasksAmountForActiveMonth = 0;
                if ($scope.calenderDayTPL.url == "templates/month.task.list.tpl.html") {
                    $scope.getTasksAmountForActiveMonth();
                }

            };
            
            $scope.goPreMonth = function goPreMonth() {

                if ($scope.currentMonth == 0) {
                    $scope.currentYear -= 1;
                }
                
                $scope.activeMonth = CalenderService.getActiveMonth($scope.currentMonth - 1);
                $scope.nextMonth = CalenderService.getMonth($scope.currentMonth);
                $scope.lastMonth = CalenderService.getMonth($scope.currentMonth - 2);
                $scope.tasksForEveryDayInActiveMonth = CalenderService.getDayArray($scope.currentYear, $scope.currentMonth - 1);
                
                $scope.currentMonth = $scope.currentMonth == 0 ? 11 : $scope.currentMonth - 1;
                
                tasks = TaskService.tasksByLane["TO DO"];
                $scope.tasksForEveryDayInActiveMonth = CalenderService.getDayArray($scope.currentYear, $scope.currentMonth);
                $scope.tasksForEveryDayInActiveMonth = TaskService.buildTaskDayRelation(
                    $scope.tasksForEveryDayInActiveMonth, tasks, $scope.currentYear, $scope.currentMonth + 1
                );
                
                $scope.activeDay = 1;
                $scope.activeDayIx = -1;

                $scope.tasksInActiveDay = $scope.tasksForEveryDayInActiveMonth.activeMonth.tasks[$scope.activeDay - 1];

                $scope.taskEditBarTPL = {
                    "url" : ""
                };
                $scope.taskEditBarTop = {
                    "top" : "775px"
                };

                $scope.tasksAmountForActiveMonth = 0;
                if ($scope.calenderDayTPL.url == "templates/month.task.list.tpl.html") {
                    $scope.getTasksAmountForActiveMonth();
                }

            };
            
            $scope.setActiveDate = function setActiveDate(index, activeDay) {
                $scope.activeDayIx = index;
                $scope.activeDay = activeDay;
                $scope.tasksInActiveDay = $scope.tasksForEveryDayInActiveMonth.activeMonth.tasks[activeDay - 1];
            };
            
            $scope.changeTaskListView = function changeTaskListView() {

                $scope.activeDay = 1;
                $scope.activeDayIx = -1;

                $scope.calenderDayTPL.url = $scope.calenderDayTPL.url == "templates/calender.widget.day.tpl.html" ?
                    "templates/month.task.list.tpl.html" : "templates/calender.widget.day.tpl.html";

                $scope.tasksAmountForActiveMonth = 0;
                if ($scope.calenderDayTPL.url == "templates/month.task.list.tpl.html") {
                    $scope.getTasksAmountForActiveMonth();
                } else {
                    $scope.tasksInActiveDay = $scope.tasksForEveryDayInActiveMonth.activeMonth.tasks[$scope.activeDay - 1];
                }

            };
            
            $scope.showTaskEditBar = function showTaskEditBar(index) {
                $scope.taskEditBarTPL = {
                    "url" : "templates/task.edit.bar.tpl.html"
                };
                $scope.taskEditBarTop = {
                    "top" : "873px"
                }
                $scope.activeTaskIndex = index;
                TaskService.activeTask = $scope.tasksInActiveDay[index];
            };
            
            $scope.createTask = function createTask(data) {

                data.title = data.description;
                data.dueDate
                    = moment($scope.activeDay + " " + (parseInt($scope.currentMonth) + parseInt(1)) + " " + $scope.currentYear, "DD MM YYYY").valueOf();

                //var date = moment();
                //data.dueDate = moment().zone("08")
                //                    .year($scope.currentYear)
                //                    .month($scope.currentMonth)
                //                    .date($scope.activeDay)
                //                    .hour(date.hour())
                //                    .minute(date.minute())
                //                    .toISOString();
                TaskService.createTask(data).then(
                    function success() {
                        $scope.tasksInActiveDay = $scope.tasksForEveryDayInActiveMonth.activeMonth.tasks[$scope.activeDay - 1];
                    }
                );
            };

            $scope.seletedTaskId = "";

            $scope.editTask = function editTask(id) {
                var selectedTask = TaskService.findTaskById(id);
                if (selectedTask != null) {
                    TaskService.activeTask = selectedTask;
                    $location.url("/td");
                }
            };

            $scope.setSelectedTaskId = function setSelectedTaskId(id) {
                $scope.seletedTaskId = id;
            };
            
        }
    ]);
})();